# Code of Conduct #

This document is expected to be constantly changing over time.  We plan on 
keeping this document updated with more specifics over time as we experience
decisions which are based on what works, and what does not work.

Our goal with this is to provide both a general outline for the policy direction
which any project we maintain will have, as well as provide specific rules so
that we can ease a decision when a situation arrises.

## Terms ##

0. These terms are intended primarly as guidelines, and not rules, as such the 
application of these as rules can be overriden on the basis that the intention
is different from what these say.
1. We do not intend to be a heirarchtical organization and as such we do not 
have any rules to define that the heirarchy overrules the community consensus
2. We do have people who are responsible for maintaining the community and they
are expected to be listened to, but are expected to listen to the community 
as a whole not just the community leadership.
3. Be nice to each other.
4. Remeber that we all make mistakes.
5. Disagree respectfully.

### Explanations ###

#### 0. Guidelines and not rules ####

This is a really important part of the way this is intended to work out.  The
priority of these terms is from highest priority to lowest priority.  0 is the
highest priority.  

These rules are not intended as a way to maintain some set of rule based
consistency.  I (Jigme Datse Yli-Rasku) have a really hard time with any kind
of system which is based on rules and this is right, and this is wrong.

So, I believe we should understand that the specifics of the rules are more as
examples of ways that things can work.

A big part of why we feel that the idea of having a rule based system is really
problematic, is that often such rule based systems allow people who are skilled
at following the rules, and getting their own way often find ways to manipulate
the rules in such a way as to be able to do stuff which is very much not in line
with the intention of the rules, but is entirely in accord with the rules 
themself.

This can easily lead to people who are working within the spirt of the rules to
end up breaking the rules themself out of frustration that the other person is
doing stuff which is against the spirt, but not actually against the rules.

So, this is important to understand.  Obeying these rules, while violating the 
spirit is *not* allowed.

#### 1. Non-heirarcial ####

The intention here is so that someone doesn't feel that they simply need to cozy
up to the people who are in the lead, and take the power away from people who
at least appear to be of, "lower status."

The intention isn't to allow the over ruling of the community care team, but 
rather that we hope to not have situations of power over, where people are 
afraid of disagreeing with members of the team on the basis that they are of
higher prestige within the community.

#### 2. Community Care Team ####

There is a community care team, and they do have the "power" to enforce these
guidelines, and will not allow discussion of the guidelines to be a factor to
increase the violation of these guidelines.  

#### 3. Be nice to one another ####

This is just a guide that the intention is that people get along.  

That said, it should be noted that being nice to one another does not mean that
you should allow yourself not express disagreement with a person who you feel
is not being nice.

Remember... we are building a community of people who can work together.  Being
nice means that there has to be an honouring of individual, and group 
boundaries.

If a person does not like being called something, then please do not call them 
that.  If you're not sure, ask.  If you get an unexpected reaction, ask.

#### 4. Remember we all make mistakes ####

Making mistakes is part of life, and this is how we learn.  If someone makes a
mistake, then be gentle, and let them know.  

If you make a mistake, again be gentle, and acknowledge your mistake.

This is how we can all grow together to be a community which we want to 
maintain.

#### 5. Disagree Respectfully ####

This is here because we do not want this to be a community where there is a 
sense that people can not disagree with each other.

Disagreeing respectufully is something which means that you actually respect 
the opinion of the other person.  This is a tricky thing to figure out what
respect actually is, and future rules will help to clarify what that will mean
as well as other aspects of this code of conduct.  